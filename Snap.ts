import { ProBuilder } from "../../ProBuilder"
import { EditableActor } from "../../EditableActor"
import Utils = require("../../Utils")

import Mesh = require("../../Mesh")
import { Face } from "../../Face"
import { Edge } from "../../Edge"
import { Vertex } from "../../Vertex"


export function Initialize() {
    console.log("InitializeSnap")
    global.ProBuilder.PluginManager.Snap = Snap
}

export function Remove() {
    global.ProBuilder.PluginManager.Snap.SnappedMeshElement = null
    global.ProBuilder.PluginManager.Snap = null
}

declare var global: {
    World: World,
    Editor: EditorEngine,
    Engine: EditorEngine,
    ProBuilder: ProBuilder,
    Compile: any,
    EditableActorClass: any
}

export interface SnapI {
    SnappedMeshElement: Mesh.MeshElement
    Snap(TraceResult: HitResult)
    GetSnappedOrPlanePosition(PlaneOrigin, PlaneNormal): [Vector, boolean, Mesh.MeshElement]
}

export module Snap {

    export let SnappedMeshElement: Mesh.MeshElement
    let SnappedVector: Vector

    const SnapDistance = 10

    export function Snap(TraceResult: HitResult) {
        SnappedMeshElement = null
        global.ProBuilder.SnappedPoint.X = global.ProBuilder.SnappedPoint.Y = -1

        if (!global.ProBuilder.CommandManager.NeedSnapping()) {
            return
        }

        // let ActorUnderCursor: EditableActor = TraceResult != null ? TraceResult.Actor : null
        // let bEditableMeshUnderCursor = ActorUnderCursor != null && ActorUnderCursor instanceof global.EditableActorClass && TraceResult.Component != null

        // if (bEditableMeshUnderCursor) {
        //     let ImpactPoint = TraceResult.ImpactPoint
        //     SnappedMeshElement = ActorUnderCursor.Mesh.GetHitElement(ImpactPoint, global.ProBuilder.GridSize)

        //     if (SnappedMeshElement != null) {
        //         global.ProBuilder.SnappedPoint = global.ProBuilder.MousePosition
        //         let Transform = ActorUnderCursor.Mesh.GetComponentToWorld()

        //         if (SnappedMeshElement instanceof Face)
        //         {
        //             SnappedVector = ImpactPoint
        //             global.ProBuilder.SnappedPointType = 0
        //         }
        //         else if (SnappedMeshElement instanceof Vertex)
        //         {
        //             SnappedVector = Transform.TransformLocation(SnappedMeshElement.GetPosition())
        //             global.ProBuilder.SnappedPointType = 3
        //         }
        //         else if (SnappedMeshElement instanceof Edge)
        //         {
        //             let MiddlePoint: Vector = Transform.TransformLocation(SnappedMeshElement.GetPosition())
        //             if (ImpactPoint.SubtractV(MiddlePoint).Size() < global.ProBuilder.GridSize)
        //             {
        //                 SnappedVector = MiddlePoint
        //                 global.ProBuilder.SnappedPointType = 2
        //             }
        //             else
        //             {
        //                 let Edge: Edge = <Edge>(SnappedMeshElement)
        //                 let ClosestPointOnEdge = Vector.ZeroVector()
        //                 let Distance = Utils.PointDistanceToLine(ImpactPoint, Transform.TransformDirection(Edge.GetDirection(false)), Transform.TransformLocation(<Vector><any>Edge.Vertices.First()), ClosestPointOnEdge)
        //                 SnappedVector = ClosestPointOnEdge
        //                 global.ProBuilder.SnappedPointType = 1
        //             }
        //         }
        //     }
        // }
        // else
        // {
        //     // ... test edges and vertices
        // }

        let ClickRectangle = Utils.Box2D.CreateFromCenterAndExtentF(global.ProBuilder.MousePosition, SnapDistance)
        let FrustumResult = global.ProBuilder.GetCameraFrustumSafe(ClickRectangle.Min, ClickRectangle.Max)
        let CameraPlanes = FrustumResult.OutPlanes

        let MouseRay = global.ProBuilder.DeprojectScreenToWorld(global.ProBuilder.MousePosition)
        let MousePositionWorld = MouseRay.OutRayOrigin

        // No need for front and back planes:
        if(FrustumResult.OutNearPlane) {
            CameraPlanes.splice(0, 1)
        }
        if(FrustumResult.OutFarPlane) {
            CameraPlanes.splice(0, 1)
        }

        let ClickFrustum = Utils.Frustum.CreateFromUEPlanes(CameraPlanes)

        for(let P of ClickFrustum.Planes) {
            P.Normal.Set(P.Normal.MultiplyF(-1))
        }

        let EAs = new Utils.TArray<EditableActor>()

        for(let EA of global.ProBuilder.GetEditableActors()) {
            if(EA.IsValid() && ClickFrustum.Intersects(EA.GetBox())) {
                EAs.Add(EA)
            }
        }

        let MinDistanceFromMouse = Utils.BIG_NUMBER
        
        SnappedVector = null
        SnappedMeshElement = null


        // Snap faces

        if( TraceResult != null && 
            TraceResult.Actor != null && 
            TraceResult.Component != null && 
            TraceResult.Actor instanceof global.EditableActorClass && 
            TraceResult.ImpactPoint != null) {
            SnappedMeshElement = TraceResult.Actor.Mesh.FaceIndexToFace.get(TraceResult.FaceIndex)
            SnappedVector = TraceResult.ImpactPoint
            global.ProBuilder.SnappedPointType = 0
            MinDistanceFromMouse = TraceResult.ImpactPoint.SubtractV(MousePositionWorld).Length()
        }

        // Snap edges

        for(let EA of EAs) {
            let ComponentToWorld = EA.Mesh.GetComponentToWorld()

            let Edges: Edge[] = <any>EA.Mesh.EdgeOctree.QueryIntersectsFrustum(ClickFrustum.Clone(), ComponentToWorld)

            for(let E of Edges) {

                let SegmentPoints = KismetMathLibrary.FindNearestPointsOnLineSegments(MousePositionWorld, 
                                                                                      MouseRay.OutRayDirection.MultiplyF(1000000), 
                                                                                      E.GetFirstVertex().ToWorld(ComponentToWorld), 
                                                                                      E.GetLastVertex().ToWorld(ComponentToWorld))

                if(SegmentPoints.Segment1Point.SubtractV(SegmentPoints.Segment2Point).Length() < SnapDistance) {
                    let Distance = SegmentPoints.Segment1Point.SubtractV(MousePositionWorld).Length()
                    let PositionOnEdge = SegmentPoints.Segment2Point
                    if(Distance < MinDistanceFromMouse + SnapDistance) {
                        SnappedMeshElement = E
                        MinDistanceFromMouse = Distance
                        let MiddlePoint: Vector = ComponentToWorld.TransformLocation(E.GetPosition())
                        if (PositionOnEdge.SubtractV(MiddlePoint).Size() < SnapDistance)
                        {
                            SnappedVector = MiddlePoint
                            global.ProBuilder.SnappedPointType = 2
                        }
                        else
                        {
                            SnappedVector = PositionOnEdge
                            global.ProBuilder.SnappedPointType = 1
                        }

                    }
                }
            }
        }

        // Snap vertices

        for(let EA of EAs) {
            let ComponentToWorld = EA.Mesh.GetComponentToWorld()

            let Vertices: Vertex[] = <any>EA.Mesh.VertexOctree.QueryIntersectsFrustum(ClickFrustum.Clone(), ComponentToWorld)

            for(let V of Vertices) {
                let VertexWorld = V.ToWorld(ComponentToWorld)
                let Distance = VertexWorld.SubtractV(MousePositionWorld).Length()
                if(Distance < MinDistanceFromMouse + SnapDistance) {
                    SnappedMeshElement = V
                    SnappedVector = VertexWorld
                    global.ProBuilder.SnappedPointType = 3
                    MinDistanceFromMouse = Distance
                }
            }

        }

        // If it was snapped on a face or an edge: try to quantize on grid, if the result gets out of the face or edge: ignore grid snap

        if(SnappedMeshElement != null) {
            let ComponentToWorld = SnappedMeshElement.Mesh.GetComponentToWorld()
            if (global.ProBuilder.GridSize > Utils.O_O1) {
                let QuantizedPosition = Utils.QuantizeRoundV(SnappedVector.Clone(), global.ProBuilder.GridSize)
                if (SnappedMeshElement instanceof Face)
                {
                    let SnappedFace = <Face>SnappedMeshElement
                    let ComponentToWorld = SnappedFace.Mesh.GetComponentToWorld()

                    if (Math.abs(QuantizedPosition.DistanceToPlane(ComponentToWorld.TransformLocation(SnappedFace.Origin), ComponentToWorld.TransformDirection(SnappedFace.Normal))) < Utils.O_O1) {
                        SnappedVector = QuantizedPosition
                    }
                }
                else if (SnappedMeshElement instanceof Edge)
                {
                    let SnappedEdge = <Edge>SnappedMeshElement
                    let ComponentToWorld = SnappedEdge.Mesh.GetComponentToWorld()

                    let ClosestPointOnEdge = Vector.ZeroVector()
                    let Distance = Utils.PointDistanceToLine(QuantizedPosition, ComponentToWorld.TransformDirection(<any>SnappedEdge.GetDirection(false)), ComponentToWorld.TransformLocation(<any>SnappedEdge.GetFirstVertex()), ClosestPointOnEdge)

                    if (!SnappedEdge.bCurved && Distance < Utils.O_O1) {
                        SnappedVector = QuantizedPosition
                    }
                }
            }

            global.ProBuilder.SnappedPoint = global.ProBuilder.ProjectWorldToScreen(SnappedVector).OutScreenPosition
        }
    }


    export function GetSnappedOrPlanePosition(PlaneOrigin, PlaneNormal): [Vector, boolean, Mesh.MeshElement] {

        if (SnappedMeshElement != null) {
            let Position = SnappedVector.Clone()
            // if (global.ProBuilder.GridSize > Utils.O_O1) {
            //     let QuantizedPosition = Utils.QuantizeRoundV(SnappedVector, global.ProBuilder.GridSize)
            //     if (SnappedMeshElement instanceof Face)
            //     {
            //         let SnappedFace = <Face>SnappedMeshElement
            //         let ComponentToWorld = SnappedFace.Mesh.GetComponentToWorld()

            //         if (Math.abs(QuantizedPosition.DistanceToPlane(ComponentToWorld.TransformLocation(SnappedFace.Origin), ComponentToWorld.TransformDirection(SnappedFace.Normal))) < Utils.O_O1) {
            //             Position = QuantizedPosition
            //         }
            //     }
            //     else if (SnappedMeshElement instanceof Edge)
            //     {
            //         let SnappedEdge = <Edge>SnappedMeshElement
            //         let ComponentToWorld = SnappedEdge.Mesh.GetComponentToWorld()

            //         let ClosestPointOnEdge = Vector.ZeroVector()
            //         let Distance = Utils.PointDistanceToLine(QuantizedPosition, ComponentToWorld.TransformLocation(<any>SnappedEdge.GetFirstVertex()), ComponentToWorld.TransformLocation(<any>SnappedEdge.GetLastVertex()), ClosestPointOnEdge)

            //         if (!SnappedEdge.bCurved && Distance < Utils.O_O1) {
            //             Position = QuantizedPosition
            //         }
            //     }
            // }
            return [Position, false, SnappedMeshElement]
        }
        else {
            return <any>global.ProBuilder.GetQuantizedMousePositionOnPlane(PlaneOrigin, PlaneNormal)
        }
    }   
}